import discord
import json
import datetime

def get_config(filename):
    try:
        with open(filename) as config_file:
            return json.load(config_file)
    except FileNotFoundError:
        print("ERROR: Couldn't find config file")
        exit(1)

config = get_config("config.json")

def get_info_embed_colour():
    return discord.Colour.from_rgb(config["embed_colour_info"][0], config["embed_colour_info"][1],
            config["embed_colour_info"][2])

def get_success_embed_colour():
    return discord.Colour.from_rgb(config["embed_colour_success"][0], config["embed_colour_success"][1],
            config["embed_colour_success"][2])

def get_error_embed_colour():
    return discord.Colour.from_rgb(config["embed_colour_error"][0], config["embed_colour_error"][1],
            config["embed_colour_error"][2])



def format_date(date):
    return date.strftime("%Y-%m-%d %H:%M")

