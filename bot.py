#!/usr/bin/env python3.6

import discord
from discord.ext import commands
import datetime
import json
import logging

from utils import *

config = get_config("config.json")

bot = commands.Bot(command_prefix = config["prefix"], status = discord.Status.idle,
        activity = discord.Game(name = "Loading..."))

logging.basicConfig(level = logging.WARNING)

info_embed_colour = get_info_embed_colour()
success_embed_colour = get_success_embed_colour()
error_embed_colour = get_error_embed_colour()


@bot.event
async def on_ready():
    print("Logged in as {0.name}#{0.discriminator} with ID {0.id}".format(bot.user))
    print("Ready...")

    await bot.change_presence(status = discord.Status.online,
            activity = discord.Game(name = "{0}help".format(config["prefix"])))


@bot.command(help = "Gets the bot's latency")
async def ping(ctx):
    em = discord.Embed(title = "**__Pong!__**",
            description = "**Latency:** {0}ms".format(round(bot.latency * 1000)),
            colour = get_info_embed_colour())

    em.set_footer(text = "Command executed by {0.name}#{0.discriminator}".format(ctx.author),
            icon_url = bot.user.avatar_url)

    await ctx.channel.send(embed = em)

@bot.event
async def on_command_error(ctx, error):
    em = discord.Embed(title = "**__Error__**", colour = error_embed_colour)

    if(isinstance(error, commands.BadArgument)):
        em.description = "An invalid argument was given."
    elif(isinstance(error, commands.MissingRequiredArgument)):
        em.description = "Missing a required argument ({0}).".format(error.param)
    elif(isinstance(error, commands.TooManyArguments)):
        em.description = "Too many arguments were given."
    elif(isinstance(error, commands.CommandOnCooldown)):
        em.description = "This command is on cooldown for another {0} seconds.".format(error.retry_after)
    elif(isinstance(error, commands.NoPrivateMessage)):
        em.description = "This command is not supported in private messages."
    elif(isinstance(error, commands.PrivateMessageOnly)):
        em.description = "This command is only supported in private messages."
    elif(isinstance(error, commands.CommandNotFound)):
        em.description = "Command not found."
    elif(isinstance(error, commands.DisabledCommand)):
        em.description = "This command is disabled."
    elif(isinstance(error, commands.MissingPermissions)):
        em.description = "You don't have permission to do that."
    elif(isinstance(error, commands.BotMissingPermissions)):
        em.description = "I don't have permission to do that."
    else:
        em.description = "{0}".format(error)

    await ctx.channel.send(embed = em)


bot.load_extension("cogs.moderation")
bot.load_extension("cogs.information")
bot.load_extension("cogs.fun")

bot.run(config["token"])

