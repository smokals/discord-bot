import discord
from discord.ext import commands

from utils import *

class Information(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.config = get_config("config.json")
        self.info_embed_colour = get_info_embed_colour()
        self.success_embed_colour = get_success_embed_colour()
        self.error_embed_colour = get_error_embed_colour()

    @commands.command(help = "Shows information about a user")
    @commands.guild_only()
    async def userinfo(self, ctx, member : discord.Member = None):
        if(member == None): # no user specified, use author
            member = ctx.author

        em = discord.Embed(title = "**__{0.name}#{0.discriminator}__**".format(member),
                colour = self.info_embed_colour)

        em.set_thumbnail(url = member.avatar_url)
        em.set_footer(text = "Command executed by {0.name}#{0.discriminator}".format(ctx.author),
                icon_url = self.bot.user.avatar_url)

        em.add_field(name = "**Account Created**", value = format_date(member.created_at), inline = True)
        em.add_field(name = "**Joined**", value = format_date(member.joined_at), inline = True)
        if(member.premium_since != None):
            em.add_field(name = "**Boosting Since**", value = format_date(member.premium_since), inline = True)
        if(member.nick != None):
            em.add_field(name = "**Nickname**", value = member.nick, inline = True)
        em.add_field(name = "**Status**", value = member.status, inline = True)
        em.add_field(name = "**Top Role**", value = member.top_role, inline = True)
        em.add_field(name = "**ID**", value = member.id, inline = True)

        await ctx.channel.send(embed = em)

    @commands.command(help = "Shows information about the server", aliases = ["guildinfo"])
    @commands.guild_only()
    async def serverinfo(self, ctx):
        em = discord.Embed(title = "**__{0}__**".format(ctx.guild.name), colour = self.info_embed_colour)

        em.set_thumbnail(url = ctx.guild.icon_url)
        em.set_footer(text = "Command executed by {0.name}#{0.discriminator}".format(ctx.author),
                icon_url = self.bot.user.avatar_url)

        em.add_field(name = "**Owner**", value = ctx.guild.owner, inline = True)
        em.add_field(name = "**Members**", value = ctx.guild.member_count, inline = True)
        em.add_field(name = "**Region**", value = ctx.guild.region, inline = True)
        em.add_field(name = "**Emojis**", value = len(ctx.guild.emojis), inline = True)
        em.add_field(name = "**Boosters**", value = ctx.guild.premium_subscription_count, inline = True)
        em.add_field(name = "**Text Channels**", value = len(ctx.guild.text_channels), inline = True)
        em.add_field(name = "**Voice Channels**", value = len(ctx.guild.voice_channels), inline = True)
        em.add_field(name = "**Roles**", value = len(ctx.guild.roles), inline = True)
        em.add_field(name = "**Created**", value = format_date(ctx.guild.created_at), inline = True)
        em.add_field(name = "**ID**", value = ctx.guild.id, inline = True)

        await ctx.channel.send(embed = em)

def setup(bot):
    bot.add_cog(Information(bot))

