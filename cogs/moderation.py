import discord
from discord.ext import commands

from utils import *

class Moderation(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.config = get_config("config.json")
        self.info_embed_colour = get_info_embed_colour()
        self.success_embed_colour = get_success_embed_colour()
        self.error_embed_colour = get_error_embed_colour()

    @commands.command(help = "Kicks a member")
    @commands.guild_only()
    @commands.has_permissions(kick_members = True)
    @commands.bot_has_permissions(kick_members = True)
    async def kick(self, ctx, member : discord.Member, *, reason = None):
        await member.kick(reason = reason) # TODO handle 403 forbidden

        em = discord.Embed(title = "**__Kicked {0.name}#{0.discriminator}__**".format(member),
                colour = self.success_embed_colour)
        if(reason != None):
            em.description = "**Reason:** `{0}`".format(reason)
        em.set_footer(text = "Command executed by {0.name}#{0.discriminator}".format(ctx.author),
                icon_url = self.bot.user.avatar_url)

        await ctx.channel.send(embed = em)


    @commands.command(help = "Bans a member")
    @commands.guild_only()
    @commands.has_permissions(ban_members = True)
    @commands.bot_has_permissions(ban_members = True)
    async def ban(self, ctx, member : discord.Member, *, reason = None):
        await member.ban(reason = reason) # TODO handle 403 forbidden

        em = discord.Embed(title = "**__Banned {0.name}#{0.discriminator}__**".format(member),
                colour = self.success_embed_colour)
        if(reason != None):
            em.description = "**Reason:** `{0}`".format(reason)
        em.set_footer(text = "Command executed by {0.name}#{0.discriminator}".format(ctx.author),
                icon_url = self.bot.user.avatar_url)

        await ctx.channel.send(embed = em)


    @commands.command(help = "Sets the nickname of a member", alises = ["nickname"])
    @commands.guild_only()
    @commands.bot_has_permissions(manage_nicknames = True)
    async def nick(self, ctx, member : discord.Member, *, nickname = None):
        if(nickname != None and len(nickname) > 32):
            pass # TODO

        em = discord.Embed(title = "**__Updated {0.name}#{0.discriminator}'s nickname__**".format(member),
                colour = self.success_embed_colour)
        em.set_footer(text = "Command executed by {0.name}#{0.discriminator}".format(ctx.author),
                icon_url = self.bot.user.avatar_url)

        # manually check the executing member's permissions to allow members to modify their own
        # nicknames with only change_nickname perms, not requiring manage_nicknames
        if(ctx.author.id == member.id): # the member is changing their own nickname
            if(ctx.author.permissions_in(ctx.channel).change_nickname): # the member has the change_nickname permission
                await member.edit(nick = nickname)
                await ctx.channel.send(embed = em)
            else:
                raise commands.errors.MissingPermissions(["change_nickname"])
        else:
            if(ctx.author.permissions_in(ctx.channel).manage_nicknames): # the member has the manage_nicknames permission
                await member.edit(nick = nickname)
                await ctx.channel.send(embed = em)
            else:
                raise commands.errors.MissingPermissions(["manage_nicknames"])


def setup(bot):
    bot.add_cog(Moderation(bot))

