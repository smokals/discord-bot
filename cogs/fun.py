import discord
from discord.ext import commands
import requests
import urllib
import json

from utils import *

class Fun(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.config = get_config("config.json")
        self.info_embed_colour = get_info_embed_colour()
        self.success_embed_colour = get_success_embed_colour()
        self.error_embed_colour = get_error_embed_colour()

    @commands.command(help = "Gets a random cat picture")
    async def cat(self, ctx):
        headers = {
            "X-API-KEY": self.config["cat_api_key"]
        }

        params = {
            "has_breeds": True,
            "mime_types": "jpg,png",
            "size": "small",
            "sub_id": ctx.author.name,
            "limit": 1
        }

        r = requests.get("https://api.thecatapi.com/v1/images/search?{0}".format(params),
                data = urllib.parse.urlencode(params), headers = headers)
        data = r.json()

        image = data[0]["url"]
        breed = data[0]["breeds"][0]

        em = discord.Embed(description = "**Breed Description:** {0}".format(breed["description"]),
                colour = self.info_embed_colour)
        em.add_field(name = "**Breed**", value = breed["name"])
        em.add_field(name = "**Temperament**", value = breed["temperament"])
        em.set_image(url = image)
        em.set_footer(text = "Command executed by {0.name}#{0.discriminator}".format(ctx.author),
                icon_url = self.bot.user.avatar_url)

        await ctx.channel.send(embed = em)

    @commands.command(help = "Gets a random dog picture")
    async def dog(self, ctx):
        headers = {
            "X-API-KEY": self.config["dog_api_key"]
        }

        params = {
            "has_breeds": True,
            "mime_types": "jpg,png",
            "size": "small",
            "sub_id": ctx.author.name,
            "limit": 1
        }

        r = requests.get("https://api.thedogapi.com/v1/images/search?{0}".format(params),
                data = urllib.parse.urlencode(params), headers = headers)
        data = r.json()

        image = data[0]["url"]
        breed = data[0]["breeds"][0]

        em = discord.Embed(colour = self.info_embed_colour)
        em.add_field(name = "**Breed**", value = breed["name"])
        em.add_field(name = "**Temperament**", value = breed["temperament"])
        em.set_image(url = image)
        em.set_footer(text = "Command executed by {0.name}#{0.discriminator}".format(ctx.author),
                icon_url = self.bot.user.avatar_url)

        await ctx.channel.send(embed = em)

def setup(bot):
    bot.add_cog(Fun(bot))

